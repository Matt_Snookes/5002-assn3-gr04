﻿using System;

using System.Collections.Generic;

namespace _5002_assn3_gr04
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
            var option = 0;
            do
            {
                Menu.PrintMenu();
                var t = Console.ReadLine();
                bool checkInput = int.TryParse(t, out option);
                if (checkInput)
                {
                    switch (option)
                    {
                        case 1:
                            LeapYears.Leap();
                            break;
                        case 2:
                            WorkingDays.Work();
                            break;
                        case 3:
                            TeacherStudent.Teach();
                            break;
                        case 4:
                            Fibonacci.Fib();
                            break;
                        default:
                            Console.WriteLine("\n\nThankyou for using Alien Software");
                            Console.WriteLine("\nPress any key to exit");
                            Console.ReadKey();
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("You didn't type in a number");
                    option = 0;
                    Console.WriteLine("Press <Enter> to quit the program");
                    Console.ReadKey();
                }
                Console.WriteLine();
            } while (option < 5);
        }
    }
}