using System;

using System.Collections.Generic;

namespace _5002_assn3_gr04
{
    class Menu
    {
        public static void PrintMenu()
        {
            Console.WriteLine("**********************************************");
            Console.WriteLine("*************   Alien Software   *************");
            Console.WriteLine("**********************************************");
            Console.WriteLine("*****            ************            *****");
            Console.WriteLine("*****          ****************          *****");
            Console.WriteLine("*****         ***     **     ***         *****");
            Console.WriteLine("*****         ***     **     ***         *****");
            Console.WriteLine("*****         ****    **    ****         *****");
            Console.WriteLine("*****          ****************          *****");
            Console.WriteLine("*****           **************           *****");
            Console.WriteLine("*****             ***    ***             *****");
            Console.WriteLine("*****               ******               *****");
            Console.WriteLine("**********************************************");
            Console.WriteLine("**********************************************");
            Console.WriteLine("*****                                    *****");
            Console.WriteLine("*****                 1:                 *****");
            Console.WriteLine("*****                                    *****");
            Console.WriteLine("*****    Leap Years between 2017-2067    *****");
            Console.WriteLine("*****                                    *****");
            Console.WriteLine("*****         By Matthew Snookes         *****");
            Console.WriteLine("*****                                    *****");
            Console.WriteLine("**********************************************");
            Console.WriteLine("**********************************************");
            Console.WriteLine("*****                                    *****");
            Console.WriteLine("*****                 2:                 *****");
            Console.WriteLine("*****                                    *****");
            Console.WriteLine("*****            Working Days            *****");
            Console.WriteLine("*****                                    *****");
            Console.WriteLine("*****          By Cullen Wilson          *****");
            Console.WriteLine("*****                                    *****");
            Console.WriteLine("**********************************************");
            Console.WriteLine("**********************************************");
            Console.WriteLine("*****                                    *****");
            Console.WriteLine("*****                 3:                 *****");
            Console.WriteLine("*****                                    *****");
            Console.WriteLine("*****        Teacher/student List        *****");
            Console.WriteLine("*****                                    *****");
            Console.WriteLine("*****          By Ethan Pearson          *****");
            Console.WriteLine("*****                                    *****");
            Console.WriteLine("**********************************************");
            Console.WriteLine("**********************************************");
            Console.WriteLine("*****                                    *****");
            Console.WriteLine("*****                 4:                 *****");
            Console.WriteLine("*****                                    *****");
            Console.WriteLine("*****             Fibonacci              *****");
            Console.WriteLine("*****                                    *****");
            Console.WriteLine("*****          By Kyrin Snookes          *****");
            Console.WriteLine("*****                                    *****");
            Console.WriteLine("**********************************************");
            Console.WriteLine("**********************************************");
            Console.WriteLine("*****                                    *****");
            Console.WriteLine("*****                 5:                 *****");
            Console.WriteLine("*****                                    *****");
            Console.WriteLine("*****            Quit Program            *****");
            Console.WriteLine("*****                                    *****");
            Console.WriteLine("**********************************************");
            Console.WriteLine("**********************************************");
            Console.WriteLine();
            Console.WriteLine("\nPlease choose a number:\n");

        }
    }
}

