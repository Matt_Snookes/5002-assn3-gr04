using System;

using System.Collections.Generic;

namespace _5002_assn3_gr04
{
    class LeapYears
    {
        public static void Leap()
        {

            Console.WriteLine($"\nYou selected option 1: Leap years between 2017-2067\n");
            for (int year = 2017; year <= 2067; year++)
            {
                if (DateTime.IsLeapYear(year))
                {
                    Console.WriteLine("{0} is a leap year.", year);
                }
            }
            Console.WriteLine("\nPress any key to return to main menu");
            Console.ReadKey();
        }
    }
}

