using System;

using System.Collections.Generic;

namespace _5002_assn3_gr04
{
    class WorkingDays
    {
        public static void Work()
        {
            bool day;
            var days = 0;
            bool hour;
            var hours = 0;
            Console.WriteLine($"\nYou selected option 2: Working Days\n");
        days:
            Console.WriteLine("How many days a week do you work?\n");
            day = Int32.TryParse(Console.ReadLine(), out days);
            if (day == false)
            {
                System.Console.WriteLine("\nPlease choose a number\n");
                goto days;
            }
            else if (day == true)
            {
                if (days > 7)
                {
                    Console.WriteLine($"\nYou entered {days} days! Your planet only has 7 days in a week, Try again earthling!\n");
                    goto days;
                }
                else
                {
                    Console.WriteLine($"\nyou entered {days} days a week");
                }
            }
        hours:
            Console.WriteLine("\nHow many hours a day do you work?\n");
            hour = Int32.TryParse(Console.ReadLine(), out hours);
            if (hour == false)
            {
                System.Console.WriteLine("\nPlease choose a number\n");
                goto hours;
            }
            if (hour == true)
            {
                var totalHours = hours * days;
                if (hours > 24)
                {
                    Console.WriteLine($"\nYou entered {hours} hours! There are only 24 hours in a day on planet earth!\n\nTry again earthling\n");
                    goto hours;
                }
                else if (totalHours == 40)
                {
                    Console.WriteLine("\n\nYou work 40 hours a week");
                }
                else if (totalHours < 40)
                {
                    Console.WriteLine($"\n\nYou work {40 - totalHours} less of 40 hours a week");
                }
                else if (totalHours > 40)
                {
                    Console.WriteLine($"\n\nYou work {totalHours - 40} hours over 40 hours a week");
                }
                Console.WriteLine($"\nYou work {totalHours} hours a week\n\nYou work {totalHours * 48} hours a year and have 4 weeks holiday");
                Console.WriteLine("\nPress any key to return to main menu");
                Console.ReadKey();
            }
        }
    }
}

