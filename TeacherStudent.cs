using System;

using System.Collections.Generic;

namespace _5002_assn3_gr04
{
    class TeacherStudent
    {
        public static void Teach()
        {
            Console.WriteLine($"\nYou selected option 3: Teacher/student List\n");
            var dict = new Dictionary<string, string>();
            dict.Add("Murray", "Teacher");
            dict.Add("Jeff", "Teacher");
            dict.Add("Stefan", "Teacher");
            dict.Add("Ray", "Teacher");
            dict.Add("John", "Teacher");
            dict.Add("Ethan", "Student");
            dict.Add("Cullen", "Student");
            dict.Add("Zac", "Student");
            dict.Add("Karan", "Student");
            dict.Add("Matt", "Student");
            dict.Add("Kyrin", "Student");
            dict.Add("Travis", "Student");
            dict.Add("Kadin", "Student");
            dict.Add("Sean", "Student");
            dict.Add("Dani", "Student");
            dict.Add("Will", "Student");
            dict.Add("Jack", "Student");
            dict.Add("Tiina", "Student");
            dict.Add("Ravjot", "Student");
            dict.Add("Jordan", "Student");
            int counter1 = 0;
            int counter2 = 0;
            foreach (var z in dict)
                if (z.Value == "Student")
                {
                    counter1++;
                }
                else if (z.Value == "Teacher")
                {
                    counter2++;
                }
            Console.WriteLine($"Number of students {counter1}\n");
            Console.WriteLine($"Number of Teachers {counter2}\n");
            Console.WriteLine("List of students:\n");
            foreach (var y in dict)
            {
                if (y.Value == "Student")
                {
                    Console.WriteLine($"{y.Key}");
                }
            }
            Console.WriteLine("\nPress any key to return to main menu");
            Console.ReadKey();
        }
    }
}