using System;

using System.Collections.Generic;

namespace _5002_assn3_gr04
{
    class Fibonacci
    {
        public static void Fib()
        {
            bool terms;
            var x = 0;
            var i = 0;
            var a = 1.0;
            var b = 1.0;
            Console.WriteLine($"\nYou selected option 4: The fibonacci sequence");
        start:
            Console.WriteLine("\nChoose the number of terms you would like to print from the fibonacci sequence\n");
            terms = Int32.TryParse(Console.ReadLine(), out x);
            if (terms == false)
            {
                System.Console.WriteLine("\nPlease choose a number");
                goto start;
            }
            else if (terms == true)
            {
                Console.WriteLine($"\nThe number of terms you've chosen is {x} and the Fibonacci numbers are:\n");
                for (i = 0; i < x; i++)
                {
                    var temp = a;
                    a = b;
                    b = temp + b;
                    Console.WriteLine($"{a}");
                }
            }
            Console.WriteLine("\nPress any key to return to main menu");
            Console.ReadKey();
        }
    }
}




